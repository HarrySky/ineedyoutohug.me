from os import getenv
from typing import Optional

from http3 import AsyncClient
from starlette.endpoints import HTTPEndpoint
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.requests import Request as StarletteRequest
from starlette import status

BOT_TOKEN = getenv("BOT_TOKEN", "12345")
CHAT_ID = getenv("CHAT_ID", "12345")
URL = f"https://api.telegram.org/bot{BOT_TOKEN}/sendMessage"

NOTIFIED = PlainTextResponse("I Am Aware Now :)")
INADEQUATE_LENGTH = PlainTextResponse("Inadequate Length!", status_code=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE)
BAD_REQUEST = PlainTextResponse("Bad Request!", status_code=status.HTTP_400_BAD_REQUEST)
TELEGRAM_PROBLEM = PlainTextResponse("Problem on Telegram Side!", status_code=status.HTTP_502_BAD_GATEWAY)

SERVER = Starlette()

@SERVER.route("/notify")
class NotifyEndpoint(HTTPEndpoint):
    _client: Optional[AsyncClient] = None

    @property
    def client(self) -> AsyncClient:
        if not self._client:
            self._client = AsyncClient()

        return self._client

    async def notify(self, message: str) -> bool:
        params = {"chat_id": CHAT_ID, "text": message}
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "User-Agent": "ineedyoutohug.me 0.0.1"
        }

        try:
            # Getting response from Telegram Bot API
            response = await self.client.get(URL, params=params, headers=headers, timeout=1)
            # Checking if everything is ok
            if not response.json()["ok"]:
                return False
        except:
            # Problems with request or JSON can occur
            return False

        return True

    async def post(self, request: StarletteRequest) -> PlainTextResponse:
        form = await request.form()
        if "message" not in form or "is_near" not in form:
            return BAD_REQUEST

        message = form["message"]
        # Can be 0 (False) or 1 (True)
        is_near = int(form["is_near"])

        if len(message) > 64:
            return INADEQUATE_LENGTH

        notification = f"""Somebody{' (near you)' if bool(is_near) else ' (somewhere)'} wants you to hug them!

Message:
{message if message else 'They preferred to remain silent'}
"""

        if not await self.notify(notification):
            return TELEGRAM_PROBLEM

        return NOTIFIED
